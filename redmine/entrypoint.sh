#!/bin/bash

# shellcheck disable=SC1091

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace # Uncomment this line for debugging purpose

# Load Redmine environment
. /opt/bitnami/scripts/redmine-env.sh

# Load libraries
. /opt/bitnami/scripts/libbitnami.sh
. /opt/bitnami/scripts/liblog.sh

print_welcome_page

cd "$REDMINE_BASE_DIR"

if [ -d "/tmp/redmine_openid_connect" ]; then
    mv /tmp/redmine_openid_connect plugins/
fi

if [ -d "plugins/redmine_openid_connect" ]; then
    mv plugins/redmine_openid_connect /tmp/
fi

if [[ "$1" = "/opt/bitnami/scripts/redmine/run.sh" ]]; then
    /opt/bitnami/scripts/mysql-client/setup.sh
    /opt/bitnami/scripts/postgresql-client/setup.sh
    /opt/bitnami/scripts/redmine/setup.sh
    /post-init.sh
    info "** Redmine setup finished! **"
fi

if [ -d "/tmp/redmine_openid_connect" ]; then
    mv /tmp/redmine_openid_connect plugins/
fi

RAILS_ENV=production bundle config unset deployment
bundle install
bundle exec rake redmine:plugins:migrate RAILS_ENV=production

echo ""
exec "$@"